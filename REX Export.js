// ==UserScript==
// @name     REX Export
// @version  1
// @grant    GM_getResourceText
// @require  http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @require  https://code.jquery.com/jquery-2.1.0.min.js
// @match    https://fixedlineremedy.enterprise.vodafone.com/*
// @grant    GM_xmlhttpRequest
// ==/UserScript==

////////////////////////////////////////////////////////////////////////////////

let isInited = false;
let isWaiting = false;
let currentInterval = []
let newChildren = []
let input = []

const [body] = document.getElementsByTagName("body");
const observerConfig = {
    attributes: true,
    attributeFilter: ["style"]
}
const observer = new MutationObserver(observerCallback);
observer.observe(body, observerConfig);

function observerCallback(mutations) {
    for(let mutation of mutations){
        isWaiting = mutation.target.style.cursor === "wait"
    }
    if(!isWaiting){
        getNewData()
        if(!isInited) isInited = true
    }
};

////////////////////////////////////////////////////////////////////////////////

// new elements
var button = $('<button id="przycisk1" type="button"  style="position: absolute; left:200px;">Export</button>');
var button3 = $('<button id="autorefresh" type="button"  style="position: absolute; left:400px;">AutoRefresh</button>');

////////////////////////////////////////////////////////////////////////////////

function toggleAutorefresh(autorefresh) {
    if (button.value == "ON") {
      button.value = "OFF";
      clearInterval(currentInterval)
    } else {
      button.value = "ON";
      currentInterval = setInterval(() =>{
        document.getElementsByClassName("Ref")[0].click()
    }, 60000); // 60000 = 60sec
    }
    alert(button.value)
} //autoclick, clicks button every XXsec

function getNewData() {
    const data = []
    const name = []

    $("#T1020 tr").each(function(x, z){
    name[x] = [];
    $(this).children('th').each(function(xx, zz){
           name[x][xx] = $(this).text();
      });
})

    $("#T1020 tr").each(function(i, v){
        data[i] = [];
        $(this).children('td').each(function(ii, vv){
            data[i][ii] = $(this).text();
      });
   })

name.splice(1)
data.shift()

const result = data.map((item) => {

  /* Reduce items of array1[0] to an object
  that corresponds to current item of array2 */
  return name[0].reduce((obj, value, index) => {

    return { ...obj,
      [value]: item[index]
    };
  }, {});

});
let tetra = []
tetra = JSON.stringify(result);

GM_xmlhttpRequest ( {
    method:     "POST",
    url:        "http://127.0.0.1:3000/index",
    data:       tetra,
    headers:    {
            "Content-Type": "application/json"
    },
    onload:     function (response) {
        $("#input").get()[0].value=response.responseText;
    }
} );
}

$(".TableHdrR").prepend(button);
$(".Toolbar").prepend(button3);

// add buttons
$("#przycisk1").click(function() {
   downloadObjectAsJson();
});

$("#autorefresh").click(function() {
    toggleAutorefresh();

});